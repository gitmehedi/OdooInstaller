This script is based on the install script from Yenthe V.G (https://github.com/Yenthe666/InstallScript)
but goes a bit further. In this script, I also added oca repository link. This script will also give you the ability to define an xmlrpc_port in the .conf file that is generated under /etc/
This script can be safely used in a multi-odoo code base server because the default Odoo port is changed BEFORE the Odoo is started.

<h3>What version to choose?</h3>
By chosing the version at the top in Github you can choose to use the script according to your need.
8.0 is specifically build for Odoo V8 installations
9.0 is specifically build for Odoo V9 installations
10.0 is specifically build for Odoo V10 installations
11.0 is specifically build for Odoo V11 installations
master is specifically build for testing & development
